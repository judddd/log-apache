package main

import (
	bytes2 "bytes"
	"compress/gzip"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit"
	"github.com/elastic/go-elasticsearch/v8/esutil"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
)

// 定义日志结构体
type LogData struct {
	Host           string `json:"host"`
	UserIdentifier string `json:"user-identifier"`
	Datetime       string `json:"datetime"`
	Method         string `json:"method"`
	Request        string `json:"request"`
	Protocol       string `json:"protocol"`
	Status         int    `json:"status"`
	Bytes          int    `json:"bytes"`
	Referer        string `json:"referer"`
}

// Generate generates the logs with given options
func Generate(option *Option) error {
	var (
		splitCount = 1
		created    = time.Now()

		interval time.Duration
		delay    time.Duration
	)

	if option.Delay > 0 {
		interval = option.Delay
		delay = interval
	}
	if option.Sleep > 0 {
		interval = option.Sleep
	}

	logFileName := option.Output
	writer, err := NewWriter(option.Type, logFileName)
	if err != nil {
		return err
	}

	if option.Forever {
		for {
			time.Sleep(delay)
			log := NewLog(option.Format, created)
			_, _ = writer.Write([]byte(log + "\n"))
			created = created.Add(interval)
		}
	}

	if option.Bytes == 0 {
		// Generates the logs until the certain number of lines is reached
		for line := 0; line < option.Number; line++ {
			time.Sleep(delay)
			log := NewLog(option.Format, created)
			_, _ = writer.Write([]byte(log + "\n"))

			if (option.Type != "stdout") && (option.SplitBy > 0) && (line > option.SplitBy*splitCount) {
				_ = writer.Close()
				fmt.Println(logFileName, "is created.")

				logFileName = NewSplitFileName(option.Output, splitCount)
				writer, _ = NewWriter(option.Type, logFileName)

				splitCount++
			}
			created = created.Add(interval)
		}
	} else {
		es, err := elasticsearch.NewClient(elasticsearch.Config{
			Addresses: []string{
				"https://localhost:9200", // 替换为你的 ES 地址
			},
			Username: "test",   // 替换为你的 ES 用户名
			Password: "123456", // 替换为你的 ES 密码
			Transport: &http.Transport{ // 跳过 TLS 认证
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		})
		if err != nil {
			log.Fatalf("Error creating the client: %s", err)
		}
		// 配置 BulkIndexer
		indexer, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
			Client:        es,
			Index:         "logs-test", // 默认索引名
			FlushBytes:    5e+6,        // 5MB 自动刷新
			NumWorkers:    5,           // 并发 worker 数量
			FlushInterval: 2 * time.Second,
		})
		if err != nil {
			log.Fatalf("Error creating the indexer: %s", err)
		}
		// Generates the logs until the certain size in bytes is reached
		bytes := 0
		var buf bytes2.Buffer
		for bytes < option.Bytes {
			if option.Format == "es" {
				buf.Reset()
				time.Sleep(delay)
				log1 := LogData{
					Host:           gofakeit.IPv4Address(),
					UserIdentifier: RandAuthUserID(),
					Datetime:       created.Format(CommonLog),
					Method:         gofakeit.HTTPMethod(),
					Request:        RandResourceURI(),
					Protocol:       RandHTTPVersion(),
					Status:         gofakeit.StatusCode(),
					Bytes:          gofakeit.Number(0, 30000),
					Referer:        gofakeit.URL(),
				}
				if err := json.NewEncoder(&buf).Encode(log1); err != nil {
					log.Fatalf("Error encoding log data: %s", err)
				}

				// 添加到 BulkIndexer 中
				err = indexer.Add(context.Background(), esutil.BulkIndexerItem{
					Action: "index",                         // 索引操作
					Body:   strings.NewReader(buf.String()), // 文档内容
					OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, resp esutil.BulkIndexerResponseItem, err error) {
						if err != nil {
							log.Printf("Error: %s", err)
						} else {
							log.Printf("Error indexing document ID %s: %s", item.DocumentID, resp.Error.Reason)
						}
					},
				})
				bytes += buf.Len()
				if err != nil {
					log.Fatalf("Error adding document to bulk indexer: %s", err)
				}

				created = created.Add(interval)
			} else {
				time.Sleep(delay)
				log := NewLog(option.Format, created)
				_, _ = writer.Write([]byte(log + "\n"))

				bytes += len(log)
				if (option.Type != "stdout") && (option.SplitBy > 0) && (bytes > option.SplitBy*splitCount+1) {
					_ = writer.Close()
					fmt.Println(logFileName, "is created.")

					logFileName = NewSplitFileName(option.Output, splitCount)
					writer, _ = NewWriter(option.Type, logFileName)

					splitCount++
				}
				created = created.Add(interval)
			}
		}

		if err := indexer.Close(context.Background()); err != nil {
			log.Fatalf("Error closing the indexer: %s", err)
		}

		// 输出统计信息
		stats := indexer.Stats()
		fmt.Printf("Indexed %d documents with %d errors\n", stats.NumFlushed, stats.NumFailed)
	}

	if option.Type != "stdout" {
		_ = writer.Close()
		fmt.Println(logFileName, "is created.")
	}
	return nil
}

// NewWriter returns a closeable writer corresponding to given log type
func NewWriter(logType string, logFileName string) (io.WriteCloser, error) {
	switch logType {
	case "stdout":
		return os.Stdout, nil
	case "log":
		logFile, err := os.Create(logFileName)
		if err != nil {
			return nil, err
		}
		return logFile, nil
	case "gz":
		logFile, err := os.Create(logFileName)
		if err != nil {
			return nil, err
		}
		return gzip.NewWriter(logFile), nil
	default:
		return nil, nil
	}
}

// NewLog creates a log for given format
func NewLog(format string, t time.Time) string {
	switch format {
	case "apache_common":
		return NewApacheCommonLog(t)
	case "apache_combined":
		return NewApacheCombinedLog(t)
	case "apache_error":
		return NewApacheErrorLog(t)
	case "rfc3164":
		return NewRFC3164Log(t)
	case "rfc5424":
		return NewRFC5424Log(t)
	case "common_log":
		return NewCommonLogFormat(t)
	case "json":
		return NewJSONLogFormat(t)
	default:
		return ""
	}
}

// NewSplitFileName creates a new file path with split count
func NewSplitFileName(path string, count int) string {
	logFileNameExt := filepath.Ext(path)
	pathWithoutExt := strings.TrimSuffix(path, logFileNameExt)
	return pathWithoutExt + strconv.Itoa(count) + logFileNameExt
}
